<pre>
  ┌───────┐
  │       │
  │  a:o  │  acolono.com
  │       │
  └───────┘
</pre>

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Installation
 * Configuration

INTRODUCTION
------------

The module introduces the `entity_reference_media` Field type, which contains:

- a field for Caption
- fields for video Start-End time

So, when re-using the same media item in different places - you can have different Caption text / Start-End time for each reference.

FEATURES
------------

- `entity_reference_media` is enhanced Drupal's `entity_reference` Field type
- contains an optimized instance of `Media Library Widget`, so this nice Media Manager works fine with new field type
- also contains an optimized instance of `Reference Entity Formatter`

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

- create a new field using `entity_reference_media`
- inside Formatter settings choose which one Field you want to be overwritten with Caption / Start-End values
- in case you need it working with other Widgets/Formatters, the patch or `hook_field_formatter_info_alter` (or similar) would be needed

by acolono GmbH
---------------

~~we build your websites~~
we build your business

hello@acolono.com

www.acolono.com
www.twitter.com/acolono
www.drupal.org/acolono-gmbh
