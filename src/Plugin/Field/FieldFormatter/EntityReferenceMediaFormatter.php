<?php

namespace Drupal\entity_reference_media\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'entity_reference_media_entity' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_media_entity",
 *   label = @Translation("Rendered entity"),
 *   description = @Translation("Display the referenced entities rendered by
 *   entity_view(), with optional field overrides."), field_types = {
 *     "entity_reference_media"
 *   }
 * )
 */
class EntityReferenceMediaFormatter extends EntityReferenceEntityFormatter {

  /**
   * The EntityField Manager service.
   */
  protected $entityFieldManager;

  /**
   * The EntityTypeBundleInfo.
   */
  protected $entityTypeBundleInfo;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->entityTypeBundleInfo = $container->get('entity_type.bundle.info');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
        'custom_caption' => '',
        'video_start' => '',
        'video_end' => '',
      ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $target_entity_type = $this->getFieldSetting('target_type');

    $mediaBundles = $this->entityTypeBundleInfo->getBundleInfo($target_entity_type);
    $options = array_combine(array_keys($mediaBundles), array_column($mediaBundles, 'label'));

    $elements['media_bundles'] = [
      '#type' => 'select',
      '#options' => $options,
      '#title' => t('Select a bundle'),
    ];

    $string = $this->entityFieldManager->getFieldMapByFieldType('string');
    $string_long = $this->entityFieldManager->getFieldMapByFieldType('string_long');
    $text = $this->entityFieldManager->getFieldMapByFieldType('text');
    $text_long = $this->entityFieldManager->getFieldMapByFieldType('text_long');
    $text_with_summary = $this->entityFieldManager->getFieldMapByFieldType('text_with_summary');

    $fieldOptions = array_merge_recursive(
      isset($string[$target_entity_type]) ? $string[$target_entity_type] : [],
      isset($string_long[$target_entity_type]) ? $string_long[$target_entity_type] : [],
      isset($text[$target_entity_type]) ? $text[$target_entity_type] : [],
      isset($text_long[$target_entity_type]) ? $text_long[$target_entity_type] : [],
      isset($text_with_summary[$target_entity_type]) ? $text_with_summary[$target_entity_type] : [],
    );

    foreach ($mediaBundles as $key => $item['label']) {
      $fields[$key][''] = t('-- Select --');

      foreach ($fieldOptions as $field => $fieldOption) {
        if (!empty($fieldOption['bundles'][$key])) {
          $fields[$key][$field] = $field;
        }
      }

      $elements['custom_caption'][$key] = [
        '#type' => 'select',
        '#options' => $fields[$key],
        '#title' => t('Custom Caption'),
        '#description' => t('Select the field you want to override with <em>custom_caption</em>'),
        '#default_value' => !empty($this->getSetting('custom_caption')[$key]) ? $this->getSetting('custom_caption')[$key] : '',
        '#access' => !empty($this->fieldDefinition->getSetting('field_caption')[$key]),
        '#states' => [
          'visible' => [
            ':input[name*="media_bundles"]' => ['value' => $key],
          ],
        ],
      ];

      $elements['video_start'][$key] = [
        '#type' => 'select',
        '#options' => $fields[$key],
        '#title' => t('Video Start'),
        '#description' => t('Select the field you want to override with <em>video_start</em>'),
        '#default_value' => !empty($this->getSetting('video_start')[$key]) ? $this->getSetting('video_start')[$key] : '',
        '#access' => !empty($this->fieldDefinition->getSetting('field_start_end')[$key]),
        '#states' => [
          'visible' => [
            ':input[name*="media_bundles"]' => ['value' => $key],
          ],
        ],
      ];

      $elements['video_end'][$key] = [
        '#type' => 'select',
        '#options' => $fields[$key],
        '#title' => t('Video End'),
        '#description' => t('Select the field you want to override with <em>video_end</em>'),
        '#default_value' => !empty($this->getSetting('video_end')[$key]) ? $this->getSetting('video_end')[$key] : '',
        '#access' => !empty($this->fieldDefinition->getSetting('field_start_end')[$key]),
        '#states' => [
          'visible' => [
            ':input[name*="media_bundles"]' => ['value' => $key],
          ],
        ],
      ];
    }

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    //    $summary[] = $this->showCaptionField() ? t('<em>custom_caption</em> overrides <em>@override</em>', ['@override' => $this->getSetting('custom_caption')]) : [];
    //    $summary[] = $this->showStartEndFields() ? t('<em>video_start</em> overrides <em>@override</em>', ['@override' => $this->getSetting('video_start')]) : [];
    //    $summary[] = $this->showStartEndFields() ? t('<em>video_end</em> overrides <em>@override</em>', ['@override' => $this->getSetting('video_end')]) : [];
    //    $summary = array_filter($summary);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $view_mode = $this->getSetting('view_mode');
    $elements = [];

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $entity) {

      $default_caption = $items->get($delta)->get('default_caption')->getCastedValue();
      $custom_caption = $items->get($delta)->get('custom_caption')->getValue();

      if (!$default_caption && $this->getSetting('custom_caption')) {
        // custom caption
        $customCaption = $this->getSetting('custom_caption')[$entity->bundle()];
        if ($entity->hasField($customCaption)) {
          $field_type = $entity->get($customCaption)->getFieldDefinition()->getType();
          $field_value = $entity->get($customCaption)->getValue();

          $override_text = $custom_caption;
          if (in_array($field_type, [
            'text',
            'text_long',
            'text_with_summary',
          ])) {
            // Get current source field format. If doesn't exist - use 'basic_html' as default
            $current_format = isset($field_value[0]['format']) ? $field_value[0]['format'] : 'basic_html';
            $override_text = [
              'value' => $custom_caption,
              'format' => $current_format,
            ];
          }

          $entity->set($customCaption, $override_text);
        }
      }

      $default_start_end = $items->get($delta)->get('default_start_end')->getCastedValue();
      $video_start = $items->get($delta)->get('video_start')->getValue();
      $video_end = $items->get($delta)->get('video_end')->getValue();

      if (!$default_start_end && $this->getSetting('video_start') && $this->getSetting('video_end')) {
        // video start
        $videoStart = $this->getSetting('video_start')[$entity->bundle()];
        if ($entity->hasField($videoStart)) {
          $entity->set($videoStart, $video_start);
        }
        // video end
        $videoEnd = $this->getSetting('video_end')[$entity->bundle()];
        if ($entity->hasField($videoEnd)) {
          $entity->set($videoEnd, $video_end);
        }
      }

      $view_builder = $this->entityTypeManager->getViewBuilder($entity->getEntityTypeId());
      $elements[$delta] = $view_builder->view($entity, $view_mode, $entity->language()
        ->getId());

      if (!$default_caption && !empty($custom_caption)) {
        $elements[$delta]['#cache']['keys'][] = md5($custom_caption);
      }

      if (!$default_start_end && !empty($video_start)) {
        $elements[$delta]['#cache']['keys'][] = md5($video_start);
      }

      if (!$default_start_end && !empty($video_end)) {
        $elements[$delta]['#cache']['keys'][] = md5($video_end);
      }

    }

    return $elements;
  }

}
