<?php

namespace Drupal\entity_reference_media\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\media_library\MediaLibraryState;
use Drupal\media_library\Plugin\Field\FieldWidget\MediaLibraryWidget;

/**
 * @FieldWidget(
 *   id = "entity_reference_media_library",
 *   label = @Translation("Media library"),
 *   description = @Translation("Allows you to select items from the media
 *   library."), field_types = {
 *     "entity_reference_media"
 *   },
 *   multiple_values = TRUE,
 * )
 */
class EntityReferenceMediaWidget extends MediaLibraryWidget {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);

    $referencedEntities = $items->referencedEntities();
    $fieldName = $this->fieldDefinition->getName();

    foreach ($referencedEntities as $delta => $mediaItem) {
      $referenceField = $items->get($delta);

      $showCaption = !empty($this->fieldDefinition->getSetting('field_caption')[$mediaItem->bundle()]);
      $showStartEnd = !empty($this->fieldDefinition->getSetting('field_start_end')[$mediaItem->bundle()]);

      $states_parents = array_merge($element["#field_parents"], [$fieldName], ['selection'], [$delta]);
      $states_prefix = array_shift($states_parents);
      $states_prefix .= '[' . implode('][', $states_parents) . ']';

      $defaultCaption = $referenceField->default_caption != NULL ? $referenceField->default_caption : TRUE;
      $defaultStartEnd = $referenceField->default_start_end != NULL ? $referenceField->default_start_end : TRUE;
      $customCaption = $referenceField->get('custom_caption')->getValue();
      $videoStart = $referenceField->get('video_start')->getValue();
      $videoEnd = $referenceField->get('video_end')->getValue();

      $element['selection'][$delta]['default_caption'] = [
        "#type" => "checkbox",
        "#title" => $this->t("Show default caption text"),
        "#default_value" => $defaultCaption,
        '#access' => $showCaption,
      ];
      $element['selection'][$delta]['custom_caption'] = [
        '#type' => 'textarea',
        '#default_value' => $customCaption,
        '#access' => $showCaption,
        '#states' => [
          'visible' => [
            ':input[name="' . $states_prefix . '[default_caption]"]' => ['unchecked' => TRUE],
          ],
        ],
      ];

      $element['selection'][$delta]['default_start_end'] = [
        "#type" => "checkbox",
        "#title" => $this->t("Use default video start/end time"),
        '#default_value' => $defaultStartEnd,
        '#access' => $showStartEnd,
      ];
      $element['selection'][$delta]['video_start'] = [
        '#type' => 'number',
        '#default_value' => !empty($videoStart) ? $videoStart : '',
        '#access' => $showStartEnd,
        '#states' => [
          'visible' => [
            ':input[name="' . $states_prefix . '[default_start_end]"]' => ['unchecked' => TRUE],
          ],
        ],
        '#attributes' => [
          'min' => '0',
          'max' => '9999999999',
        ],
      ];
      $element['selection'][$delta]['video_end'] = [
        '#type' => 'number',
        '#default_value' => !empty($videoEnd) ? $videoEnd : '',
        '#access' => $showStartEnd,
        '#states' => [
          'visible' => [
            ':input[name="' . $states_prefix . '[default_start_end]"]' => ['unchecked' => TRUE],
          ],
        ],
        '#attributes' => [
          'min' => '0',
          'max' => '9999999999',
        ],
      ];

      // move label a bit down, as it overlaps new fields
      $element["selection"][$delta]["#attributes"]["class"][] = 'media-library-item__preview';
    }

    $mediaLibraryState = $element["open_button"]["#media_library_state"];
    $allowedMediaTypeIds = $mediaLibraryState->getAllowedTypeIds();
    $selectedTypeId = $mediaLibraryState->getSelectedTypeId();
    $remaining = $mediaLibraryState->getAvailableSlots();
    $openerParameters = $mediaLibraryState->getOpenerParameters();
    $mediaLibraryNewState = MediaLibraryState::create('entity_reference_media.opener.field_widget', $allowedMediaTypeIds, $selectedTypeId, $remaining, $openerParameters);
    $element["open_button"]["#media_library_state"] = $mediaLibraryNewState;

    return $element;
  }

}
