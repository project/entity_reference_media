<?php

namespace Drupal\entity_reference_media\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\TypedData\DataDefinition;

/**
 * @FieldType(
 *     id = "entity_reference_media",
 *     label = @Translation("Media Enhanced"),
 *     description = @Translation("Entity reference media with caption text"),
 *     category = @Translation("Reference"),
 *     default_widget = "entity_reference_media_library",
 *     default_formatter = "entity_reference_media_entity",
 *     list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 * )
 */
class EntityReferenceMedia extends EntityReferenceItem {

  /**
   * {@inheritDoc}
   */
  public static function defaultStorageSettings() {
    return [
        'target_type' => 'media',
      ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritDoc}
   */
  public static function defaultFieldSettings() {
    return [
        'field_caption' => TRUE,
        'field_start_end' => FALSE,
      ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritDoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::fieldSettingsForm($form, $form_state);
    $settings = $this->getSettings();

    $handlerSettings = $this->getSetting('handler_settings');
    $mediaTypes = !empty($handlerSettings['target_bundles']) ? $handlerSettings['target_bundles'] : [];

    if ($form_state->isProcessingInput()) {
      $mediaTypes = $form_state->getUserInput()['settings']['handler_settings']['target_bundles'];
      $mediaTypes = array_filter($mediaTypes);
    }

    $element['handler']['field_caption'] = [
      '#type' => 'checkboxes',
      '#title' => t('Enable <em>Caption</em> field for:'),
      '#default_value' => (array) $settings['field_caption'],
      '#options' => $mediaTypes,
      '#description' => t('If you want to use <em>Caption</em> fields with this field, enable it here.'),
      '#access' => !empty($mediaTypes),
    ];

    $element['handler']['field_start_end'] = [
      '#type' => 'checkboxes',
      '#title' => t('Enable <em>Start/End time</em> fields for:'),
      '#default_value' => (array) $settings['field_start_end'],
      '#options' => $mediaTypes,
      '#description' => t('If you want to use <em>Start/End time</em> fields with this field, enable it here.'),
      '#access' => !empty($mediaTypes),
    ];

    return $element;
  }

  /**
   * {@inheritDoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    $properties['default_caption'] = DataDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Show default caption'))
      ->setRequired(FALSE);

    $properties['custom_caption'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Custom caption text'))
      ->setRequired(FALSE);

    $properties['default_start_end'] = DataDefinition::create('boolean')
      ->setLabel(new TranslatableMarkup('Use default video start/end time'))
      ->setRequired(FALSE);

    $properties['video_start'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Video start'))
      ->setRequired(FALSE);

    $properties['video_end'] = DataDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Video end'))
      ->setRequired(FALSE);

    return $properties;
  }

  /**
   * {@inheritDoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);

    $schema['columns']['default_caption'] = [
      'type' => 'int',
      'size' => 'tiny',
    ];

    $schema['columns']['custom_caption'] = [
      'type' => 'text',
      'size' => 'big',
    ];

    $schema['columns']['default_start_end'] = [
      'type' => 'int',
      'size' => 'tiny',
    ];

    $schema['columns']['video_start'] = [
      'type' => 'float',
      'size' => 'normal',
    ];

    $schema['columns']['video_end'] = [
      'type' => 'float',
      'size' => 'normal',
    ];

    return $schema;
  }

  /**
   * {@inheritDoc}
   */
  public static function getPreconfiguredOptions() {
    return [];
  }

}
