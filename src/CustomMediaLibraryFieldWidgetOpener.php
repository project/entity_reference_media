<?php

namespace Drupal\entity_reference_media;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\RefinableCacheableDependencyInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\media_library\MediaLibraryFieldWidgetOpener;
use Drupal\media_library\MediaLibraryState;

class CustomMediaLibraryFieldWidgetOpener extends MediaLibraryFieldWidgetOpener {

  /**
   * Override checkAccess() method and allow to be opened by an entity
   * reference media field
   *
   * @todo keep all this here until
   *   https://www.drupal.org/project/drupal/issues/3179868 is resolved
   */
  public function checkAccess(MediaLibraryState $state, AccountInterface $account) {
    $parameters = $state->getOpenerParameters() + ['entity_id' => NULL];

    $process_result = function ($result) {
      if ($result instanceof RefinableCacheableDependencyInterface) {
        $result->addCacheContexts(['url.query_args']);
      }
      return $result;
    };

    // Forbid access if any of the required parameters are missing.
    foreach (['entity_type_id', 'bundle', 'field_name'] as $key) {
      if (empty($parameters[$key])) {
        return $process_result(AccessResult::forbidden("$key parameter is missing."));
      }
    }

    $entity_type_id = $parameters['entity_type_id'];
    $bundle = $parameters['bundle'];
    $field_name = $parameters['field_name'];

    // Since we defer to a field to determine access, ensure we are dealing with
    // a fieldable entity type.
    $entity_type = $this->entityTypeManager->getDefinition($entity_type_id);
    if (!$entity_type->entityClassImplements(FieldableEntityInterface::class)) {
      throw new \LogicException("The media library can only be opened by fieldable entities.");
    }

    $storage = $this->entityTypeManager->getStorage($entity_type_id);
    $access_handler = $this->entityTypeManager->getAccessControlHandler($entity_type_id);

    if ($parameters['entity_id']) {
      $entity = $storage->load($parameters['entity_id']);
      $entity_access = $access_handler->access($entity, 'update', $account, TRUE);
    }
    else {
      $entity_access = $access_handler->createAccess($bundle, $account, [], TRUE);
    }

    // If entity-level access is denied, there's no point in continuing.
    if (!$entity_access->isAllowed()) {
      return $process_result($entity_access);
    }

    // If the entity has not been loaded, create it in memory now.
    if (!isset($entity)) {
      $values = [];
      if ($bundle_key = $entity_type->getKey('bundle')) {
        $values[$bundle_key] = $bundle;
      }
      /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
      $entity = $storage->create($values);
    }

    $items = $entity->get($field_name);
    $field_definition = $items->getFieldDefinition();

    if ($field_definition->getType() !== 'entity_reference' && $field_definition->getType() !== 'entity_reference_media') {
      throw new \LogicException('Expected the media library to be opened by an entity reference field.');
    }
    if ($field_definition->getFieldStorageDefinition()
        ->getSetting('target_type') !== 'media') {
      throw new \LogicException('Expected the media library to be opened by an entity reference field that target media items.');
    }

    $field_access = $access_handler->fieldAccess('edit', $field_definition, $account, $items, TRUE);
    return $process_result($entity_access->andIf($field_access));
  }

}
